export interface Section {
    id: string;
    title: string;
    formFields: Array<FormFields>;
    subSections: Array<{}>;
}

export interface FormFields {
    title: string;
}

export interface SubSections {
    id: string;
    title: string;
}