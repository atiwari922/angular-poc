import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-sub-section',
  templateUrl: './sub-section.component.html',
  styleUrls: ['./sub-section.component.scss'],
})
export class SubSectionComponent implements OnInit {
  @Input() subSection: any;

  selectedIndex: number;

  data = { title: 'Label' };

  constructor() {}

  ngOnInit(): void {}

  public setRow(index: number): void {
    this.selectedIndex = index;
  }
  addFields(): void {
    if (this.subSection.formFields.length < 10) {
      this.subSection.formFields.push(this.data);
    } else {
      return;
    }
  }

  deleteFields(): void {
    this.subSection.formFields.splice(-1, 1);
  }
}
