import { Component, OnInit, Input, Inject } from '@angular/core';
import { Section } from '../section/section';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.scss'],
})
export class MainContentComponent implements OnInit {
  @Input() section: Section[];

  @Input() activeSection: Section[];

  constructor() {}

  ngOnInit(): void {
    console.log(this.activeSection);
  }
}
